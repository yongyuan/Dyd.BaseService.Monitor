﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Monitor.Domain.PlatformManage.Dal;
using Monitor.Domain.UnityLog.Dal;
using Monitor.Tasks.Tool;
using BSF.Db;


namespace Monitor.Tasks
{
    /// <summary>
    /// 错误预警任务
    /// 建议cron： 1 0/5 * * * ?
    /// 描述:用于出错频率的检查及预警,当间隔时间内发生的错误数量超过指定数值时，则报错。
    /// </summary>
    public class ErrorLogWarningTask : BSF.BaseService.TaskManager.BaseDllTask
    {
        public override void TestRun()
        {
            this.AppConfig = new BSF.BaseService.TaskManager.SystemRuntime.TaskAppConfigInfo();
            this.AppConfig.Add("MonitorPlatformManageConnectString", "server=192.168.17.201;Initial Catalog=dyd_bs_monitor_platform_manage;User ID=sa;Password=Xx~!@#;");
            this.AppConfig.Add("EveryGrowErrorNum", "20");

            string json = new BSF.Serialization.JsonProvider().Serializer(this.AppConfig);

            base.TestRun();
        }

        private string UnityLogConnectString = "";
        private DateTime lastscantime = DateTime.Now;
        public override void Run()
        {
            GlobalConfig.MonitorPlatformManageConnectString = AppConfig["MonitorPlatformManageConnectString"];
            int EveryGrowErrorNum = Convert.ToInt32( AppConfig["EveryGrowErrorNum"]);

            SqlHelper.ExcuteSql(GlobalConfig.MonitorPlatformManageConnectString, (c) =>
            {
                tb_database_config_dal dal = new tb_database_config_dal();
                var list = dal.GetModelList(c);
                UnityLogConnectString = BSF.BaseService.Monitor.SystemRuntime.DbShardingHelper.GetDataBase(list, BSF.BaseService.Monitor.SystemRuntime.DataBaseType.UnityLog);
            });

            int errornum = 0;
            SqlHelper.ExcuteSql(UnityLogConnectString, (c) =>
            {
                tb_error_log_dal dal = new tb_error_log_dal();
                errornum = dal.GetGrawErrorNum(c, lastscantime);
            });
            if (errornum > EveryGrowErrorNum)
            {
                TaskLogHelper.Error("错误预警任务", string.Format("【{0}】-【{1}】期间平台错误日志增加{2}条",lastscantime,DateTime.Now,errornum), "错误预警任务");
            }
            lastscantime = DateTime.Now;
        }
    }
}
